package main;

import constants.CharacterPrinterConstants;
import service.impl.CharacterPrinterImpl;
import service.CharacterPrinterService;
import lombok.RequiredArgsConstructor;
import request.UserInputRequest;

import java.util.Scanner;

@RequiredArgsConstructor
public class Main {

    private final CharacterPrinterService characterPrinterService;

    public static void main(String[] args) throws Exception {

        CharacterPrinterService characterPrinterService = new CharacterPrinterImpl();
        Main main = new Main(characterPrinterService);
        main.callCharacterPrinter();
    }

    private void callCharacterPrinter() throws Exception {

        Scanner scanner = new Scanner(System.in);
        UserInputRequest userInputRequest = new UserInputRequest();

        System.out.println("Enter a character [O, X, Y, Z]: ");
        userInputRequest.setCharacterInput(scanner.next().charAt(CharacterPrinterConstants.firstCharacter));

        System.out.println("Enter a non-negative odd integer: ");
        userInputRequest.setSizeInput(scanner.nextInt());

        characterPrinterService.CharacterPrinter(userInputRequest);
    }
}