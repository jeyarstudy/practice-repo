package module.patternprinter;

public abstract class PatternPrinter {

    /**
     * A method for printing the O pattern
     */
    public void printOPattern(Integer size) {

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i == 0 || j == 0 || i == size - 1
                        || j == size - 1) {
                    System.out.print("*");
                }
                else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    /**
     * Method for printing the X pattern
     */
    public void printXPattern(Integer size) {

        for (int i = 1; i <= size; i++) {
            for (int j = 1; j <= size; j++) {
                if (i == j || i == (size - j + 1)) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    /**
     * Method for printing the Y pattern
     */
    public void printYPattern(Integer size) {

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (j == i && j <= size / 2) {
                    System.out.print("*");
                } else if (j == size / 2 && i >= size / 2) {
                    System.out.print("*");
                } else if (j == size - 1 - i && j >= size / 2) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    /**
     * Method for printing the Z pattern
     */
    public void printZPattern(Integer size) {
        for (int i = 0; i < size; i++) {
            if (i == 0 || i == size - 1) {
                for (int j = 0; j < size; j++) {
                    System.out.println("*");
                }
            } else {
                for (int j = 0; j < size - i - 1; j++) {
                    System.out.print(" ");
                }
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
